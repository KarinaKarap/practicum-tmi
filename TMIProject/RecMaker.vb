﻿Public Class RecMaker
    Public Shared Function generateRectangles(quantity As Integer) As List(Of RectangleObj)
        Dim random As New Random
        Dim list As New List(Of RectangleObj)
        For i = 1 To quantity
            Dim x1 = random.Next(0, 1000) / 1000
            Dim x2 = random.Next(0, 1000) / 1000
            Dim y1 = random.Next(0, 1000) / 1000
            Dim y2 = random.Next(0, 1000) / 1000

            Dim leftDown As New PointObj(Math.Min(x1, x2), Math.Min(y1, y2))
            Dim rightUp As New PointObj(Math.Max(x1, x2), Math.Max(y1, y2))

            Dim rec As New RectangleObj(leftDown, rightUp)
            list.Add(rec)
        Next
        Return list
    End Function

    Public Shared Function generateRectangles(quantity As Integer, maxWidth As Double) As List(Of RectangleObj)
        Dim random As New Random
        Dim list As New List(Of RectangleObj)
        For i = 1 To quantity
            Dim x1 = random.Next(0, (1 - maxWidth) * 1000) / 1000
            Dim y1 = random.Next(0, (1 - maxWidth) * 1000) / 1000
            Dim width = random.Next(0, maxWidth * 1000) / 1000
            Dim height = random.Next(0, maxWidth * 1000) / 1000

            Dim leftDown As New PointObj(x1, y1)
            Dim rightUp As New PointObj(x1 + width, y1 + height)

            Dim rec As New RectangleObj(leftDown, rightUp)
            list.Add(rec)
        Next
        Return list
    End Function


End Class
