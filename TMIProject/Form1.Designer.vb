﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_To = New System.Windows.Forms.Button()
        Me.btn_From = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_To = New System.Windows.Forms.TextBox()
        Me.txt_From = New System.Windows.Forms.TextBox()
        Me.pnl_Rectangles = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chb_alg3 = New System.Windows.Forms.CheckBox()
        Me.chb_alg2 = New System.Windows.Forms.CheckBox()
        Me.chb_alg1 = New System.Windows.Forms.CheckBox()
        Me.btn_Start = New System.Windows.Forms.Button()
        Me.btn_Reset = New System.Windows.Forms.Button()
        Me.timer_alg = New System.Windows.Forms.Timer(Me.components)
        Me.btn_randomRectangles = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txt_quantityRec = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_time = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btn_To)
        Me.GroupBox1.Controls.Add(Me.btn_From)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txt_To)
        Me.GroupBox1.Controls.Add(Me.txt_From)
        Me.GroupBox1.Location = New System.Drawing.Point(509, 10)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(204, 104)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "File"
        '
        'btn_To
        '
        Me.btn_To.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_To.Location = New System.Drawing.Point(174, 66)
        Me.btn_To.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_To.Name = "btn_To"
        Me.btn_To.Size = New System.Drawing.Size(16, 18)
        Me.btn_To.TabIndex = 7
        Me.btn_To.Text = "..."
        Me.btn_To.UseVisualStyleBackColor = True
        '
        'btn_From
        '
        Me.btn_From.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_From.Location = New System.Drawing.Point(174, 27)
        Me.btn_From.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_From.Name = "btn_From"
        Me.btn_From.Size = New System.Drawing.Size(16, 18)
        Me.btn_From.TabIndex = 6
        Me.btn_From.Text = "..."
        Me.btn_From.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 68)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(20, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "To"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 29)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "From"
        '
        'txt_To
        '
        Me.txt_To.Location = New System.Drawing.Point(55, 66)
        Me.txt_To.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_To.Name = "txt_To"
        Me.txt_To.Size = New System.Drawing.Size(116, 20)
        Me.txt_To.TabIndex = 1
        '
        'txt_From
        '
        Me.txt_From.Location = New System.Drawing.Point(55, 27)
        Me.txt_From.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_From.Name = "txt_From"
        Me.txt_From.Size = New System.Drawing.Size(116, 20)
        Me.txt_From.TabIndex = 0
        '
        'pnl_Rectangles
        '
        Me.pnl_Rectangles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl_Rectangles.BackColor = System.Drawing.SystemColors.ControlLight
        Me.pnl_Rectangles.Location = New System.Drawing.Point(10, 10)
        Me.pnl_Rectangles.Margin = New System.Windows.Forms.Padding(2)
        Me.pnl_Rectangles.Name = "pnl_Rectangles"
        Me.pnl_Rectangles.Size = New System.Drawing.Size(488, 431)
        Me.pnl_Rectangles.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.chb_alg3)
        Me.GroupBox2.Controls.Add(Me.chb_alg2)
        Me.GroupBox2.Controls.Add(Me.chb_alg1)
        Me.GroupBox2.Location = New System.Drawing.Point(509, 119)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(204, 127)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Algorithm"
        '
        'chb_alg3
        '
        Me.chb_alg3.AutoSize = True
        Me.chb_alg3.Location = New System.Drawing.Point(29, 91)
        Me.chb_alg3.Margin = New System.Windows.Forms.Padding(2)
        Me.chb_alg3.Name = "chb_alg3"
        Me.chb_alg3.Size = New System.Drawing.Size(61, 17)
        Me.chb_alg3.TabIndex = 2
        Me.chb_alg3.Text = "Difficult"
        Me.chb_alg3.UseVisualStyleBackColor = True
        '
        'chb_alg2
        '
        Me.chb_alg2.AutoSize = True
        Me.chb_alg2.Location = New System.Drawing.Point(29, 60)
        Me.chb_alg2.Margin = New System.Windows.Forms.Padding(2)
        Me.chb_alg2.Name = "chb_alg2"
        Me.chb_alg2.Size = New System.Drawing.Size(63, 17)
        Me.chb_alg2.TabIndex = 1
        Me.chb_alg2.Text = "Medium"
        Me.chb_alg2.UseVisualStyleBackColor = True
        '
        'chb_alg1
        '
        Me.chb_alg1.AutoSize = True
        Me.chb_alg1.Location = New System.Drawing.Point(29, 28)
        Me.chb_alg1.Margin = New System.Windows.Forms.Padding(2)
        Me.chb_alg1.Name = "chb_alg1"
        Me.chb_alg1.Size = New System.Drawing.Size(49, 17)
        Me.chb_alg1.TabIndex = 0
        Me.chb_alg1.Text = "Easy"
        Me.chb_alg1.UseVisualStyleBackColor = True
        '
        'btn_Start
        '
        Me.btn_Start.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Start.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Start.Location = New System.Drawing.Point(580, 391)
        Me.btn_Start.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Start.Name = "btn_Start"
        Me.btn_Start.Size = New System.Drawing.Size(134, 50)
        Me.btn_Start.TabIndex = 5
        Me.btn_Start.Text = "Start"
        Me.btn_Start.UseVisualStyleBackColor = True
        '
        'btn_Reset
        '
        Me.btn_Reset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Reset.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Reset.Location = New System.Drawing.Point(509, 391)
        Me.btn_Reset.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Reset.Name = "btn_Reset"
        Me.btn_Reset.Size = New System.Drawing.Size(66, 50)
        Me.btn_Reset.TabIndex = 6
        Me.btn_Reset.Text = "Reset"
        Me.btn_Reset.UseVisualStyleBackColor = True
        '
        'timer_alg
        '
        '
        'btn_randomRectangles
        '
        Me.btn_randomRectangles.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_randomRectangles.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_randomRectangles.Location = New System.Drawing.Point(98, 27)
        Me.btn_randomRectangles.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_randomRectangles.Name = "btn_randomRectangles"
        Me.btn_randomRectangles.Size = New System.Drawing.Size(93, 28)
        Me.btn_randomRectangles.TabIndex = 7
        Me.btn_randomRectangles.Text = "Generate"
        Me.btn_randomRectangles.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.txt_quantityRec)
        Me.GroupBox3.Controls.Add(Me.btn_randomRectangles)
        Me.GroupBox3.Location = New System.Drawing.Point(509, 250)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Size = New System.Drawing.Size(204, 71)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Random rectangles"
        '
        'txt_quantityRec
        '
        Me.txt_quantityRec.Location = New System.Drawing.Point(26, 33)
        Me.txt_quantityRec.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_quantityRec.Name = "txt_quantityRec"
        Me.txt_quantityRec.Size = New System.Drawing.Size(62, 20)
        Me.txt_quantityRec.TabIndex = 8
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.lbl_time)
        Me.GroupBox4.Location = New System.Drawing.Point(509, 326)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Size = New System.Drawing.Size(204, 60)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Timer"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(94, 24)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "milliseconds"
        '
        'lbl_time
        '
        Me.lbl_time.AutoSize = True
        Me.lbl_time.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_time.Location = New System.Drawing.Point(40, 24)
        Me.lbl_time.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_time.Name = "lbl_time"
        Me.lbl_time.Size = New System.Drawing.Size(18, 20)
        Me.lbl_time.TabIndex = 0
        Me.lbl_time.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 457)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btn_Reset)
        Me.Controls.Add(Me.btn_Start)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.pnl_Rectangles)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(740, 495)
        Me.Name = "Form1"
        Me.Text = "Rectangle-Intersection Algorithms"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_To As System.Windows.Forms.TextBox
    Friend WithEvents txt_From As System.Windows.Forms.TextBox
    Friend WithEvents pnl_Rectangles As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Start As System.Windows.Forms.Button
    Friend WithEvents btn_To As System.Windows.Forms.Button
    Friend WithEvents btn_From As System.Windows.Forms.Button
    Friend WithEvents chb_alg3 As System.Windows.Forms.CheckBox
    Friend WithEvents chb_alg2 As System.Windows.Forms.CheckBox
    Friend WithEvents chb_alg1 As System.Windows.Forms.CheckBox
    Friend WithEvents btn_Reset As System.Windows.Forms.Button
    Friend WithEvents timer_alg As System.Windows.Forms.Timer
    Friend WithEvents btn_randomRectangles As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_quantityRec As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_time As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label

End Class
