﻿Public Interface RecSolver
    Sub solve(rectangles As List(Of RectangleObj))
    Function getResults() As List(Of PointObj)
End Interface
