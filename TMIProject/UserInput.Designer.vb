﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.inputTextBox = New System.Windows.Forms.TextBox()
        Me.selectFile = New System.Windows.Forms.Button()
        Me.selectedFilesList = New System.Windows.Forms.ListView()
        Me.starProcess = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'inputTextBox
        '
        Me.inputTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.inputTextBox.Location = New System.Drawing.Point(28, 24)
        Me.inputTextBox.Name = "inputTextBox"
        Me.inputTextBox.Size = New System.Drawing.Size(316, 20)
        Me.inputTextBox.TabIndex = 0
        '
        'selectFile
        '
        Me.selectFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.selectFile.Location = New System.Drawing.Point(363, 21)
        Me.selectFile.Name = "selectFile"
        Me.selectFile.Size = New System.Drawing.Size(98, 23)
        Me.selectFile.TabIndex = 2
        Me.selectFile.Text = "Select File"
        Me.selectFile.UseVisualStyleBackColor = True
        '
        'selectedFilesList
        '
        Me.selectedFilesList.Alignment = System.Windows.Forms.ListViewAlignment.Left
        Me.selectedFilesList.AutoArrange = False
        Me.selectedFilesList.Location = New System.Drawing.Point(28, 66)
        Me.selectedFilesList.Name = "selectedFilesList"
        Me.selectedFilesList.Size = New System.Drawing.Size(433, 335)
        Me.selectedFilesList.TabIndex = 3
        Me.selectedFilesList.UseCompatibleStateImageBehavior = False
        '
        'starProcess
        '
        Me.starProcess.Location = New System.Drawing.Point(363, 418)
        Me.starProcess.Name = "starProcess"
        Me.starProcess.Size = New System.Drawing.Size(98, 23)
        Me.starProcess.TabIndex = 4
        Me.starProcess.Text = "Start"
        Me.starProcess.UseVisualStyleBackColor = True
        '
        'UserInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 462)
        Me.Controls.Add(Me.starProcess)
        Me.Controls.Add(Me.selectedFilesList)
        Me.Controls.Add(Me.selectFile)
        Me.Controls.Add(Me.inputTextBox)
        Me.MaximumSize = New System.Drawing.Size(500, 500)
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "UserInput"
        Me.Text = "UserInput"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents inputTextBox As System.Windows.Forms.TextBox
    Friend WithEvents selectFile As System.Windows.Forms.Button
    Friend WithEvents selectedFilesList As System.Windows.Forms.ListView
    Friend WithEvents starProcess As System.Windows.Forms.Button
End Class
