﻿
Public Class BestSweepLine : Implements RecSolver

#Region "Subclasses"
    Public Enum PointType
        startPoint
        endPoint
        topPoint
        bottomPoint
    End Enum

    Private Class xValue
        Public Sub New(x As Double, rec As RectangleObj, type As PointType)
            _X = x
            _Rectangle = rec
            _Type = type
        End Sub

        Public Property X As Double
        Public Property Rectangle As RectangleObj
        Public Property Type As PointType
    End Class

    Private Class yValue

        Public Sub New(rec As RectangleObj, type As PointType)
            _Rectangle = rec
            _Type = type
        End Sub
        Public Property Rectangle As RectangleObj
        Public Property Type As PointType
    End Class

    Private Class YComp : Implements IComparer

        Public Function Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
            If (DirectCast(x, PointObj).yCoordinate < DirectCast(y, PointObj).yCoordinate) Then
                Return -1
            ElseIf (DirectCast(x, PointObj).yCoordinate > DirectCast(y, PointObj).yCoordinate) Then
                Return 1
            Else
                If (DirectCast(x, PointObj).xCoordinate < DirectCast(y, PointObj).xCoordinate) Then
                    Return -1
                ElseIf (DirectCast(x, PointObj).xCoordinate > DirectCast(y, PointObj).xCoordinate) Then
                    Return 1
                Else
                    If (x.GetHashCode() < y.GetHashCode()) Then
                        Return -1
                    ElseIf (x.GetHashCode() > y.GetHashCode()) Then
                        Return 1
                    Else
                        Return 0
                    End If
                End If
            End If
        End Function
    End Class

    Private Class XComp : Implements IComparer

        Public Function Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
            If (DirectCast(x, PointObj).xCoordinate < DirectCast(y, PointObj).xCoordinate) Then
                Return -1
            ElseIf (DirectCast(x, PointObj).xCoordinate > DirectCast(y, PointObj).xCoordinate) Then
                Return 1
            Else
                If (DirectCast(x, PointObj).yCoordinate < DirectCast(y, PointObj).yCoordinate) Then
                    Return -1
                ElseIf (DirectCast(x, PointObj).yCoordinate > DirectCast(y, PointObj).yCoordinate) Then
                    Return 1
                Else
                    If (x.GetHashCode() < y.GetHashCode()) Then
                        Return -1
                    ElseIf (x.GetHashCode() > y.GetHashCode()) Then
                        Return 1
                    Else
                        Return 0
                    End If
                End If
            End If
        End Function
    End Class


#End Region
    Private xValues As New SortedList(New XComp, 1)
    Private yValues As New SortedList(New YComp, 1)
    Private intersections As New List(Of PointObj)

    Public Function getResults() As List(Of PointObj) Implements RecSolver.getResults
        Return intersections
    End Function

    Public Sub solve(rectangles As List(Of RectangleObj)) Implements RecSolver.solve
        addXValues(rectangles)
        While xValues.Count <> 0

            Dim minXValue As xValue = popMinXValue()

            Select Case minXValue.Type

                Case PointType.startPoint
                    addYValues(minXValue.Rectangle)
                    intersections.AddRange(findIntersectionAtXValue(minXValue.Rectangle, minXValue.X))
                    Exit Select

                Case PointType.endPoint
                    intersections.AddRange(findIntersectionAtXValue(minXValue.Rectangle, minXValue.X))
                    deleteYValues(minXValue.Rectangle)
                    Exit Select

            End Select
        End While

    End Sub

    Private Sub addXValues(rectangles As List(Of RectangleObj))
        For Each obj In rectangles
            xValues.Add(obj.leftDown, New xValue(obj.leftDown.xCoordinate, obj, PointType.startPoint))
            xValues.Add(obj.rightUp, New xValue(obj.rightUp.xCoordinate, obj, PointType.endPoint))
        Next
    End Sub

    Private Function popMinXValue()
        Dim value As xValue = DirectCast(xValues.GetByIndex(0), xValue)
        xValues.RemoveAt(0)
        Return value
    End Function

    Private Sub addYValues(rec As RectangleObj)
        yValues.Add(rec.leftDown, New yValue(rec, PointType.bottomPoint))
        yValues.Add(rec.rightUp, New yValue(rec, PointType.topPoint))
    End Sub

    Private Sub deleteYValues(rec As RectangleObj)
        yValues.Remove(rec.leftDown)
        yValues.Remove(rec.rightUp)
    End Sub

    Private Function findIntersectionAtXValue(rect As RectangleObj, x As Double) As List(Of PointObj)
        Dim currentY = rect.leftDown
        Dim lastRectangle = rect
        Dim foundIntersections = New List(Of PointObj)

        Dim previousIndex = yValues.IndexOfKey(currentY) - 1
        If (previousIndex >= 0) Then
            Dim previousKey = DirectCast(yValues.GetKey(previousIndex), PointObj)
            Dim previousObject = DirectCast(yValues.GetByIndex(previousIndex), yValue)
            If (previousKey.yCoordinate = currentY.yCoordinate) Then
                foundIntersections.Add(New PointObj(currentY.xCoordinate, currentY.yCoordinate))
            End If
        End If


        While (currentY.yCoordinate <= rect.rightUp.yCoordinate)
            Dim nextIndex = yValues.IndexOfKey(currentY) + 1

            If nextIndex >= yValues.Count Then
                Exit While
            End If

            currentY = DirectCast(yValues.GetKey(nextIndex), PointObj)

            lastRectangle = DirectCast(yValues.GetByIndex(nextIndex), yValue).Rectangle

            If lastRectangle.Equals(rect) Then
                Exit While
            End If

            foundIntersections.Add(New PointObj(x, currentY.yCoordinate))

        End While

        Dim lastIndex = yValues.IndexOfKey(rect.rightUp) + 1
        If (lastIndex < yValues.Count) Then
            Dim lastKey = DirectCast(yValues.GetKey(lastIndex), PointObj)
            Dim lastObject = DirectCast(yValues.GetByIndex(lastIndex), yValue)
            If (lastKey.yCoordinate = rect.rightUp.yCoordinate) Then
                foundIntersections.Add(New PointObj(x, rect.rightUp.yCoordinate))
            End If
        End If

        Return foundIntersections
    End Function


End Class

