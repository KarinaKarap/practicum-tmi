﻿Imports System.IO

Module main
    Sub Main()

        Dim size = 0.2
        Dim amount = 10
        Try
            While amount <= 800
                While size <= 1
                    For alg As Integer = 1 To 3
                        Dim sw = My.Computer.FileSystem.OpenTextFileWriter("C:\Users\Tom Houben\Documents\RectangleFiles\File_" + (size * 1000).ToString() + "_" + amount.ToString() + "_" + alg.ToString() + ".txt", True)
                        Dim rectangles = RecMaker.generateRectangles(amount, size)

                        sw.WriteLine(alg)
                        sw.WriteLine(amount)

                        For Each obj As RectangleObj In rectangles
                            sw.WriteLine(obj.leftDown.xCoordinate.ToString() + " " + obj.leftDown.yCoordinate.ToString() + " " + _
                                         obj.rightUp.xCoordinate.ToString() + " " + obj.rightUp.yCoordinate.ToString())
                        Next
                        sw.Close()
                        sw.Dispose()
                    Next
                    size = size + 0.2
                End While
                amount = amount * 2
            End While
        Catch e As Exception
            MsgBox(e.Message)
        End Try






    End Sub
End Module

