﻿Public Class PointObj

    Private _xCoordinate As Double
    Public Property xCoordinate
        Get
            Return _xCoordinate
        End Get
        Set(value)
            If value >= 0 And value <= 1 Then
                _xCoordinate = value
            End If
        End Set
    End Property

    Private _yCoordinate As Double
    Public Property yCoordinate
        Get
            Return _yCoordinate
        End Get
        Set(value)
            If value >= 0 And value <= 1 Then
                _yCoordinate = value
            End If
        End Set
    End Property

    Public Sub New(ByVal x As Double, ByVal y As Double)
        xCoordinate = x
        yCoordinate = y
    End Sub

End Class
