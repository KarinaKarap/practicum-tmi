﻿Imports System.IO
Imports System.Globalization

Public Class UserInput

    Public Sub inputClick(sender As Object, e As EventArgs) Handles selectFile.Click
        If directorySelector.ShowDialog() = DialogResult.OK Then
            If directorySelector.SelectedPath.Equals("") Then
                MsgBox("No directory selected!", MsgBoxStyle.Critical)
            Else
                inputTextBox.Text = directorySelector.SelectedPath
                listFilesInView()
            End If
        End If
    End Sub

    Public Sub listFilesInView()
        Dim files = Directory.GetFiles(directorySelector.SelectedPath)
        selectedFilesList.View = View.List
        selectedFilesList.Items.Clear()

        For Each file As String In files
            If file IsNot Nothing Then
                selectedFilesList.Items.Add(file)
            End If
        Next
    End Sub

    Private Function retrieveData(file As String) As List(Of RectangleObj)
        Dim ciClone As CultureInfo = CType(CultureInfo.InvariantCulture.Clone(), CultureInfo)
        ciClone.NumberFormat.NumberDecimalSeparator = "."
            'make an array with all rectangles
        Try
            Using sr As New StreamReader(file)
                Dim rectangles As New List(Of RectangleObj)
                Dim algorithm = sr.ReadLine()
                Dim amountRect = sr.ReadLine()

                Select Case algorithm
                    Case "1"
                        solver = New Easy()
                    Case "2"
                        solver = New WorstSweepLine()
                    Case "3"
                        solver = New BestSweepLine()
                End Select

                For i = 0 To amountRect - 1
                    Dim line = Split(sr.ReadLine(), " ", 4)
                    Dim leftDown = New PointObj(Convert.ToDouble(line(0), ciClone), _
                                                Convert.ToDouble(line(1), ciClone))
                    Dim rightUp = New PointObj(Convert.ToDouble(line(2), ciClone), _
                                                Convert.ToDouble(line(3), ciClone))
                    rectangles.Add(New RectangleObj(leftDown, rightUp))
                Next

                sr.Close()
                Return rectangles
            End Using

        Catch e As Exception
            Return Nothing
        End Try


    End Function

    Private Sub startSolving() Handles starProcess.Click
        Dim solutionDirectory = directorySelector.SelectedPath + "\Solution"

        If (Not Directory.Exists(directorySelector.SelectedPath + "\Solution")) Then
            Directory.CreateDirectory(solutionDirectory)
        Else
            MsgBox("Solution Directory already exists, please delete before continue!", MsgBoxStyle.Critical)
            Exit Sub
        End If


        For Each item As ListViewItem In selectedFilesList.Items
            Dim rectangles As New List(Of RectangleObj)
            rectangles.Clear()
            Try
                rectangles = retrieveData(item.Text)
            Catch e As Exception
                item.BackColor = Color.Orange
            End Try

            If rectangles IsNot Nothing Then
                Dim watch = New Stopwatch()
                watch.Reset()
                watch.Start()
                solver.solve(rectangles)
                watch.Stop()

                Dim filename = Path.GetFileNameWithoutExtension(item.Text)

                Dim sw As StreamWriter
                sw = My.Computer.FileSystem.OpenTextFileWriter(solutionDirectory + "\" + filename + "_solution.txt", True)

                For Each obj As PointObj In solver.getResults
                    sw.WriteLine(obj.xCoordinate.ToString() + " " + obj.yCoordinate.ToString())
                Next

                sw.WriteLine("")
                sw.WriteLine(watch.ElapsedMilliseconds)
                item.BackColor = Color.Green
                sw.Close()
                rectangles = Nothing
            Else
                item.BackColor = Color.Red
            End If

        Next

    End Sub

    Private directorySelector As New FolderBrowserDialog()
    Private solver As RecSolver
End Class