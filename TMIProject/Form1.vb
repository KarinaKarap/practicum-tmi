﻿Imports System.IO
Imports System.Globalization

Public Class Form1

#Region "Variables"

    Private _chosenAlgoritme As Integer
    Private _amountRectangles As Integer
    Private _Rectangles As New List(Of RectangleObj)
    Private minY As Double = 0
    Private intersections As New List(Of PointObj)
    Private bmp As New Bitmap(Me.Width, Me.Height)

#End Region

#Region "Clicks"

    Private Sub btn_From_Click(sender As Object, e As EventArgs) Handles btn_From.Click
        Dim myFileDlog As New OpenFileDialog()

        'look for files in the c drive
        myFileDlog.InitialDirectory = "documents\"

        'specifies what type of data files to look for
        myFileDlog.Filter = "All Files (*.*)|*.*" & "|Text Files (*.txt)|*.txt"

        'specifies which data type is focused on start up
        myFileDlog.FilterIndex = 2

        'Gets or sets a value indicating whether the dialog box restores the current directory before closing.
        myFileDlog.RestoreDirectory = True

        'seperates message outputs for files found or not found
        If myFileDlog.ShowDialog() = DialogResult.OK Then
            If Dir(myFileDlog.FileName) = "" Then
                MsgBox("File Not Found", MsgBoxStyle.Critical)
            End If
        End If

        'Adds the file directory to the text box
        txt_From.Text = myFileDlog.FileName
        retrieveData()
        drawRectangles()
    End Sub

    Private Sub btn_To_Click(sender As Object, e As EventArgs) Handles btn_To.Click
        Dim myFileDlog As New OpenFileDialog()

        'look for files in the c drive
        myFileDlog.InitialDirectory = "documents\"

        'specifies what type of data files to look for
        myFileDlog.Filter = "All Files (*.*)|*.*" & "|Text Files (*.txt)|*.txt"

        'specifies which data type is focused on start up
        myFileDlog.FilterIndex = 2

        'Gets or sets a value indicating whether the dialog box restores the current directory before closing.
        myFileDlog.RestoreDirectory = True

        'seperates message outputs for files found or not found
        If myFileDlog.ShowDialog() = DialogResult.OK Then
            If Dir(myFileDlog.FileName) = "" Then
                MsgBox("File Not Found", MsgBoxStyle.Critical)
            End If
        End If

        'Adds the file directory to the text box
        txt_To.Text = myFileDlog.FileName
    End Sub

    Private Sub btn_Reset_Click(sender As Object, e As EventArgs) Handles btn_Reset.Click
        _Rectangles = New List(Of RectangleObj)
        intersections = New List(Of PointObj)
        drawRectangles()
        txt_From.Text = ""
        txt_To.Text = ""
        chb_alg1.Checked = False
        chb_alg2.Checked = False
        chb_alg3.Checked = False
        lbl_time.Text = "0"
        'stopTimer()
        'resetTimer()
    End Sub

    Private Sub btn_Start_Click(sender As Object, e As EventArgs) Handles btn_Start.Click
        'timer_alg.Start()
        If chb_alg1.Checked Then
            Dim easy = New Easy()
            Dim watch = New Stopwatch()
            watch.Start()
            easy.solve(_Rectangles)
            watch.Stop()
            lbl_time.Text = watch.ElapsedMilliseconds
            intersections = easy.getResults()
            drawRectangles()
        End If
        If chb_alg2.Checked Then
            Dim algo = New WorstSweepLine()
            Dim watch = Stopwatch.StartNew()
            algo.solve(_Rectangles)
            watch.Stop()
            intersections = algo.getResults()
            lbl_time.Text = watch.ElapsedMilliseconds
            drawRectangles()
        End If
        If chb_alg3.Checked Then
            Dim algo = New BestSweepLine()
            Dim watch = Stopwatch.StartNew()
            algo.solve(_Rectangles)
            watch.Stop()
            intersections = algo.getResults()
            lbl_time.Text = watch.ElapsedMilliseconds
            drawRectangles()
        End If

    End Sub

    Private Sub btn_randomRectangles_Click(sender As Object, e As EventArgs) Handles btn_randomRectangles.Click
        Dim i = txt_quantityRec.Text
        Dim number As Integer
        If (Int32.TryParse(i, number)) Then
            _Rectangles = RecMaker.generateRectangles(number)
            drawRectangles()
        End If

    End Sub

#End Region

    Private Sub retrieveData()
        Dim ciClone As CultureInfo = CType(CultureInfo.InvariantCulture.Clone(), CultureInfo)
        ciClone.NumberFormat.NumberDecimalSeparator = "."

        'make an array with all rectangles
        Try
            Using sr As New StreamReader(txt_From.Text)
                Dim rectangles As New List(Of RectangleObj)
                _chosenAlgoritme = sr.ReadLine()
                _amountRectangles = sr.ReadLine()
                For i = 0 To _amountRectangles - 1
                    Dim line As String
                    line = sr.ReadLine()
                    Dim leftDown = New PointObj(Convert.ToDouble(line.Substring(0, 5), ciClone), _
                                                Convert.ToDouble(line.Substring(6, 5), ciClone))
                    Dim rightUp = New PointObj(Convert.ToDouble(line.Substring(12, 5), ciClone), _
                                                Convert.ToDouble(line.Substring(18, 5), ciClone))
                    rectangles.Add(New RectangleObj(leftDown, rightUp))
                Next
                _Rectangles = rectangles
            End Using
        Catch e As Exception
            MsgBox(e.Message)
        End Try

        'get the minY 
        Dim currentMinY As Double = 0
        If _Rectangles.Count <> 0 Then
            For Each rec In _Rectangles
                Dim y As Double = rec.rightUp.yCoordinate
                If (currentMinY < y) Then
                    currentMinY = y
                End If
            Next
            minY = currentMinY
        End If

        'check Algorithm
        Select Case _chosenAlgoritme
            Case 1 : chb_alg1.Checked = True
            Case 2 : chb_alg2.Checked = True
            Case 3 : chb_alg1.Checked = True
        End Select

    End Sub

    Private Sub drawRectangles()
        Using myPen As New System.Drawing.Pen(System.Drawing.Color.MidnightBlue)

            Dim constant As Integer = 500

            Dim bmp As New Bitmap(Me.Width, Me.Height)
            Using g As Graphics = Graphics.FromImage(bmp)
                For Each obj In _Rectangles
                    g.DrawRectangle(myPen, New Rectangle((obj.leftDown.xCoordinate) * constant, _
                                                        (obj.leftDown.yCoordinate) * constant, _
                                                        (obj.rightUp.xCoordinate - obj.leftDown.xCoordinate) * constant, _
                                                        (obj.rightUp.yCoordinate - obj.leftDown.yCoordinate) * constant))
                Next
                If intersections.Count > 0 Then
                    Using myPen2 As New System.Drawing.Pen(System.Drawing.Color.Red)
                        For Each obj In intersections
                            Dim ptn = New Point(obj.xCoordinate * constant, obj.yCoordinate * constant)
                            g.DrawRectangle(myPen2, New Rectangle(ptn.X, ptn.Y, 2, 2))
                        Next
                    End Using
                End If
            End Using
            pnl_Rectangles.BackgroundImage = bmp
        End Using
    End Sub

    Private Sub txt_From_TextChanged(sender As Object, e As KeyEventArgs) Handles txt_From.KeyDown
        If e.KeyCode = Keys.Enter Then
            retrieveData()
            drawRectangles()
        End If
    End Sub

    Private Sub btn_Start_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

#Region "Timer"

    'Private Sub startTimer()
    '    timer_alg.Start()
    '    lbl_Time.Text = timer_alg.ToString
    'End Sub

    'Private Sub stopTimer()
    '    timer_alg.Stop()
    'End Sub

    'Private Sub resetTimer()
    '    lbl_Time.Text = "0"
    'End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_alg.Tick
        'lbl_Time.Text = Convert.ToInt32(lbl_Time.Text) + 1
    End Sub

#End Region

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        timer_alg.Dispose()
    End Sub

End Class
