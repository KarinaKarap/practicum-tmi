﻿Public Class RectangleObj

    Private _leftDown As PointObj
    Public Property leftDown As PointObj
        Get
            Return _leftDown
        End Get
        Set(value As PointObj)
            _leftDown = value
        End Set
    End Property
    
    Private _rightUp As PointObj
    Public Property rightUp As PointObj
        Get
            Return _rightUp
        End Get
        Set(value As PointObj)
            _rightUp = value
        End Set
    End Property

    Public Sub New(ByVal leftDown As PointObj, ByVal rightUp As PointObj)
        Me.leftDown = leftDown
        Me.rightUp = rightUp
    End Sub
End Class
