﻿Imports TMIProject.PointObj

Public Class Easy : Implements RecSolver

    Dim intersections As New List(Of PointObj)

    Public Function getResults() As List(Of PointObj) Implements RecSolver.getResults
        Return intersections
    End Function

    Public Sub solve(rectangles As List(Of RectangleObj)) Implements RecSolver.solve

        For i = 0 To rectangles.Count - 1
            For j = i + 1 To rectangles.Count - 1
                If intersection(rectangles.Item(i), rectangles.Item(j)) Then
                    intersections.AddRange(findIntersection(rectangles.Item(i), rectangles.Item(j)))
                End If
            Next
        Next
    End Sub

    Private Function intersection(rec1 As RectangleObj, rec2 As RectangleObj) As Boolean
        ' rec1 right of rec2
        If rec1.leftDown.xCoordinate > rec2.rightUp.xCoordinate Then Return False
        ' rec2 right of rec1
        If rec2.leftDown.xCoordinate > rec1.rightUp.xCoordinate Then Return False
        ' rec1 under rec2
        If rec1.rightUp.yCoordinate < rec2.leftDown.yCoordinate Then Return False
        ' rec2 under rec1
        If rec2.rightUp.yCoordinate < rec1.leftDown.yCoordinate Then Return False
        ' rec1 inside of rec2
        If rec1.leftDown.xCoordinate > rec2.leftDown.xCoordinate And rec1.rightUp.xCoordinate < rec2.rightUp.xCoordinate _
            And rec1.leftDown.yCoordinate > rec2.leftDown.yCoordinate And rec1.rightUp.yCoordinate < rec2.rightUp.yCoordinate Then
            Return False
        End If
        ' rec2 inside of rec1
        If rec1.leftDown.xCoordinate < rec2.leftDown.xCoordinate And rec1.rightUp.xCoordinate > rec2.rightUp.xCoordinate _
            And rec1.leftDown.yCoordinate < rec2.leftDown.yCoordinate And rec1.rightUp.yCoordinate > rec2.rightUp.yCoordinate Then
            Return False
        End If
        Return True
    End Function

    Private Function findIntersection(rec1 As RectangleObj, rec2 As RectangleObj) As List(Of PointObj)
        Dim x1 = Math.Max(rec1.leftDown.xCoordinate, rec2.leftDown.xCoordinate)
        Dim y1 = Math.Max(rec1.leftDown.yCoordinate, rec2.leftDown.yCoordinate)
        Dim x2 = Math.Min(rec1.rightUp.xCoordinate, rec2.rightUp.xCoordinate)
        Dim y2 = Math.Min(rec1.rightUp.yCoordinate, rec2.rightUp.yCoordinate)



        Dim results As New List(Of PointObj)
        results.Add(New PointObj(x1, y1))
        results.Add(New PointObj(x2, y2))

        If rec2.rightUp.yCoordinate = y1 Or rec2.rightUp.xCoordinate = x2 _
            Or rec2.leftDown.xCoordinate = x2 Or rec2.leftDown.yCoordinate = y1 Then
            results.Add(New PointObj(x2, y1))
        End If
        If rec1.leftDown.yCoordinate = y2 Or rec1.leftDown.xCoordinate = x1 _
            Or rec1.rightUp.yCoordinate = y2 Or rec1.rightUp.xCoordinate = x1 Then
            results.Add(New PointObj(x1, y2))
        End If
        Return results
    End Function
End Class
